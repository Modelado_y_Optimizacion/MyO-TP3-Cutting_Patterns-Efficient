
# ****************************************************************************
# CASES

# Variacion :
* Cantidad de rollitos total (cant. rollitos).
* Cantidad de demanda por rollito.
* Ancho rollitos, Se aleja de anchoRollo.

* Variacion en Cantidad total de rollitos
| 2 | 4 | 8 | 16 |

# Casos

* case : variacion de rollitos totales.
* case : variacion de demanda por rollito.
* case : variacion de ancho rollitos con respecto a ancho rollo.

# ****************************************************************************
# ALGUNAS CONCLUSIONES

# TP3 : Influye la cantidad de variables (Bin e Int) sobre las cuales el modelo hara el trabajo, el tiempo que tarda en ejecutarse.
    * + cantidad de rollitos (total) + variables enteras.
    * + demanda por rollito, + cantidadMaxRollos y por lo tanto + variables binarias y variables enteras.
    * cualquiera de los 2 (TP1 o TP2) le gana al TP3.

# TP2 : 
    * Le gana al TP1 cuando ancho rollito es chico y demanda es grande, cuando hay mucho rollitos (totales).

# ****************************************************************************
# RESULTADOS.

# CASES 1X - Variamos la cantidad de rollitos => TP3 pierde siempre.

# case10 - 2 rollitos
TPN1 : 0.0397448540 segundos. (G) - Cantidad de rollos minimo:  330
TPN2 : 0.0647838116 segundos. - Cantidad de rollos minimo:  330
TPN3 : 1.4549996853 segundos. - Cantidad de rollos minimo:  330

# case11 - 4 rollitos
TPN1 : 0.0377774239 segundos. (G) - Cantidad de rollos minimo:  379
TPN2 : 0.1014726162 segundos. - Cantidad de rollos minimo:  379
TPN3 : 3.6967685223 segundos. - Cantidad de rollos minimo:  379

# case12 - 8 rollitos
TPN1 : 0.1375799179 segundos. (G) - Cantidad de rollos minimo:  527
TPN2 : 0.5462992191 segundos. - Cantidad de rollos minimo:  527
TPN3 : ...

# case13 - 16 rollitos
TPN1 : 0.0468626022 segundos. (G) - Cantidad de rollos minimo:  1818
TPN2 : 1.4144563675 segundos. - Cantidad de rollos minimo:  1819
TPN3 : ...

Podemos observar que el a medida que incrementamos los rollitos, aumenta cantidad de variables que se usan cobre el modeloAllInOne. Esto hace que tarde mucho en ejecutarse ya que el solver tiene mas variables sobre cuales evaluar.
Cabe aclarar que a lo largo de todas las pruebas realizadas el TP3, siempre perdia con respecto al TP1 y TP2 ya que como se vera mas adelante a mayor cantidad de demanda de rollitos o la variacion del ancho de los mismo influye sobre la cantidad de variables sobre el modelo y por lo tanto el tiempo del mismo.
Otra de las cosas que se noto, es que el tiempo varia exponencialmente para cualquiera de los test realizados. Por lo tanto la conclusion a la cual se llego para este modelo es que sirve cuando la entrada es muy chica (menos rollos, anchoRollito proximo a anchoRollo y cantidad total de rollitos es poco) y no se tiene o se sabe de un algoritmo que me genere la combinacion de cortes o patrones sobre los rollos. Por otro lado este modelo no gana en tiempo en ningun momento al TP1 y TP2, haciendo que este modelo tenga mucha limitacion.

# CASES 2X - Variamos el ancho rollito 9, 10 y 11 => TP2 gana a TP1.

# case20
TPN1 : 0.0624814034 segundos. (G) - Cantidad de rollos minimo:  13761
TPN2 : 0.6999113560 segundos. - Cantidad de rollos minimo:  13761
TPN3 : ...

# case21
TPN1 : 0.0625560284 segundos. (G) - Cantidad de rollos minimo:  10751
TPN2 : 0.6404199600 segundos. - Cantidad de rollos minimo:  10751
TPN3 : ...

# case22
TPN1 : 1.6409120560 segundos. (G) - Cantidad de rollos minimo:  1799
TPN2 : 2.6866014004 segundos. - Cantidad de rollos minimo:  1800
TPN3 : ...

# case23
TPN1 : 10.1711034775 segundos. - Cantidad de rollos minimo:  1224
TPN2 : 1.5307588577 segundos. (G) - Cantidad de rollos minimo:  1227
TPN3 : ...

Podemos ver que variar el ancho del rollito hace que el TP2 le pueda ganar al TP1. Esto sucede porque el algoritmo de generar los patrones maximales del TP1 demora bastante ya que es recursivo y toma 
en cuenta todos los casos o cortes sin omitir ninguno. Tambien podemos decir que la cantidad de rollitos total y la demanda de cada uno influye sobre el tiempo de ejecucion del algoritmo.

Otra cosa que notamos es que tanto en el case 22, 23, 13, el valor objetivo es distinto. Esto sucede porque el TP1 genera todos los patrones maximales, sin importar si se usan o no. En cambio en el TP2 se obtiene de manera indirecta los patrones a traves de los modelos dual y aux. haciendo que no contemple algunos patrones que si lo hace el TP1 y por lo tanto se pierden algunas soluciones que el TP1 los tiene y por eso nos puede dar o no una mejor solucion el TP1.

# CASES 3X - Variamos la demanda del rollito 7 y 8 => TP2 gana a TP1.

# case30
TPN1 : 0.2148184776 segundos. (G) - Cantidad de rollos minimo:  505
TPN2 : 0.6002202034 segundos. - Cantidad de rollos minimo:  505
TPN3 : ...

# case31
TPN1 : 0.2245163918 segundos. (G) - Cantidad de rollos minimo:  505
TPN2 : 0.6050529480 segundos. - Cantidad de rollos minimo:  505
TPN3 : ...

# case32
TPN1 : 0.5496795177 segundos. - Cantidad de rollos minimo:  505
TPN2 : 0.4250104427 segundos. (G) - Cantidad de rollos minimo:  534
TPN3 : ...

# case33
TPN1 : 1.9296123981 segundos. - Cantidad de rollos minimo:  564
TPN2 : 0.3794598579 segundos. (G) - Cantidad de rollos minimo:  589
TPN3 : ...

Podemos ver que a mayor demanda (considerando anchos de rollitos pequeños respecto al ancho del rollo) el algoritmo del TP1 se vuelve cada vez menos eficiendo ganando en un momento dado el TP2.

# ****************************************************************************
