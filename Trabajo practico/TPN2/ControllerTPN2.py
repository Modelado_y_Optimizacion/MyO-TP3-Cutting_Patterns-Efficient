
from TPN2.ModeloAuxPDC import *
from TPN2.ModeloPrimalPDC import *
from TPN2.ModeloDualPDC import *

import numpy as np
from time import time

# ****************************************************************************
# ******************************* Sub - Metodos ******************************
# ****************************************************************************
class ControllerTPN2:
    
    def seguirIterando(newPatron, ySolution):
        indices = range(len(newPatron))
        suma=0
        for i in indices:
            suma = suma + newPatron[i]*ySolution[i]
        #print("Si es >1 seguimos, sino (<=1) paramos.")
        #print("valor: "+str(suma));
        if(suma>1):    
            #print("Seguimos generando patrones.");
            return True
        else:
            #print("Paramos de generar.");
            return False

    # ****************************************************************************
    # ***************************** Metodo Principal *****************************
    # ****************************************************************************
    def principalMethod(anchoRollo, indiceAncho, indiceCantidad, listIndices, cantidad):
        #Creamos el modelo del dual y permitimos modificarlo desde el principio.
        modelDual = ModeloDualPDC.crearModelDual()
        modelAux = ModeloAuxPDC.crearModelAux()

        # Inicializamos el bucle.
        listPatronesPrimal=[]
        idPatron = 0
        listPatronesDual=[]
        y = ModeloDualPDC.resolverDual(modelDual, listPatronesDual, indiceCantidad)
        ySolution = list(zip(listIndices, y))
        newPatron = ModeloAuxPDC.resolverAuxiliar(modelAux, indiceAncho, ySolution, anchoRollo)
        newIndicePatron = list(zip(listIndices, newPatron))

        # ****************************************************************************

        #Bucle que se encarga de cargar los patrones
        while ControllerTPN2.seguirIterando(newPatron, y):
            newPatron.insert(0, idPatron)
            listPatronesPrimal.append(newPatron)
            listPatronesDual.append(newIndicePatron)
            y = ModeloDualPDC.resolverDual(modelDual, listPatronesDual, indiceCantidad)
            ySolution = list(zip(listIndices, y))
            newPatron = ModeloAuxPDC.resolverAuxiliar(modelAux, indiceAncho, ySolution, anchoRollo)
            newIndicePatron = list(zip(listIndices, newPatron))
            idPatron = idPatron + 1

        # ****************************************************************************

        # Cuando ya no tenemos que iterar mas (Sum(u*.y*)<=1 no es un patron que mejora lo que tenemos) Resolvemos el primal.
        #separador();
        ModeloPrimalPDC.resolverPrimal(listPatronesPrimal,cantidad)
