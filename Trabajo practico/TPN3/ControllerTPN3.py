
from ModeloAllInOne import *

import numpy as np
from time import time

# ****************************************************************************
# ***************************** Metodo Principal *****************************
# ****************************************************************************

# Iniciamos el timer
start_time = time()

# Declaracion de inputs.
anchoRollo = 0;
cantidad = [];
ancho = [];

# Lectura de inputs.
f = open('input.txt','r')
message = f.read()                  ## Leemos el archivo
message = message.rsplit('\n')
contador=1;
for linea in message:
    if(contador==2):
        anchoRollo = int(linea[0:len(linea)]);
    if(contador>=4):
        indice_separator = linea.index(':');
        cant = linea[0:indice_separator];
        anch =  linea[indice_separator+1:len(linea)];
        cantidad.append(int(cant));
        ancho.append(int(anch));
    contador=contador+1;
f.close()

# Ordenamos los inputs (cantidad:ancho) en base a ancho.
tupla = list(zip(ancho, cantidad));
tuplaOrdenada = sorted(tupla, reverse=True);
ancho, cantidad = zip(*tuplaOrdenada)

indices = range(len(cantidad));
listIndices = [];
for i in indices:
    listIndices.append(i);

indiceCantidad = list(zip(listIndices, cantidad));
indiceAncho = list(zip(listIndices, ancho));

#print("* Ancho Rollo: ", anchoRollo);
#print("* Ancho: ", ancho);
#print("* Cantidad: ", cantidad);
#print("* Indice-Cantidad", indiceCantidad); 
#print("* Indice-Ancho", indiceAncho); 

cantidadMaxRollos = 0;

for i in indices:
    cantidadMaxRollos = cantidadMaxRollos + math.ceil(cantidad[i]/int(anchoRollo/ancho[i]));

print();
print("Cantidad de variables binarias : ", cantidadMaxRollos);
print("Cantidad de variables enteras : ", cantidadMaxRollos * len(ancho));
print("Cantidad de variables sobre el modelo : ", cantidadMaxRollos + cantidadMaxRollos * len(ancho));
print();

# ****************************************************************************
# CODE HERE
ModeloAllInOne.resolver(anchoRollo, indiceAncho, indiceCantidad, cantidadMaxRollos);
# ****************************************************************************

execute_time = time() - start_time;
print("Tiempo de ejecucion transcurrido : %.10f segundos." % execute_time);

# ****************************************************************************
# ****************************************************************************