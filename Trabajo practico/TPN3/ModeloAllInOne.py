
# *********************************************************************************************************************************
# CONSIGNA
# Cuál es la mínima cantidad de rollos de 3 metros de ancho que necesitamos fabricar para satisfacer la siguiente demanda?
# 25 rollos de 250 cm de ancho.
# 97 rollos de 135 cm de ancho.
# 610 rollos de 108 cm de ancho.
# 395 rollos de 93 cm de ancho.
# 211 rollos de 42 cm de ancho.
# *********************************************************************************************************************************

from pyscipopt import Model, quicksum

class ModeloAllInOne:
    
    @staticmethod
    def resolver(anchoRollo, indiceAncho, indiceCantidad, maximo):
        
        # Modelo
        model = Model("TP3 : Patrones de cortes")  

        # Indices
        cantidadDeJ = range(maximo);
        cantidadDeI = range(len(indiceAncho));

        # Variables
        y = [model.addVar(vtype="BINARY") for j in cantidadDeJ]

        x = {}
        for i in cantidadDeI:
            for j in cantidadDeJ:
                x[i,j] = model.addVar(vtype="INTEGER", name="x(%s,%s)"%(i,j))

        # Funcion objetivo
        model.setObjective(sum(y), sense="minimize");

        # Restricciones

        # asociacion de variables y rollos a usar
        for j in cantidadDeJ:
            model.addCons( quicksum(indanc[1] * x[indanc[0], j] for indanc in indiceAncho) <= anchoRollo *  y[j], "Rest. Nº y["+str(j)+"]" );
        # cubrir demanda de cada rollito
        for indcan in indiceCantidad:
            model.addCons( quicksum(x[indcan[0], j] for j in cantidadDeJ) >= indcan[1], "Rest. demanda rollito ["+str(indcan[0])+"]" );

        # Optimizacion
        model.optimize()
        sol = model.getBestSol()
          
        '''
        # Ver los cortes/patrones que se usan, y que genero el modelo claro
        for j in cantidadDeJ:
            if sol[y[j]] == 1:
                patron = [];
                for i in cantidadDeI:
                    patron.append(int(sol[x[i,j]]));
                print(patron)
        '''

        suma=0;
        # Mostrar solucion
        for j in cantidadDeJ:
            suma = suma + int(sol[y[j]]);

        print("Cantidad de rollos minimo: ", int(suma));

# *********************************************************************************************************************************
