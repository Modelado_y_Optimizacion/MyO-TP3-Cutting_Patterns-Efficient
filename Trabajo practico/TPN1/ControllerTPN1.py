
from TPN1.ModeloPDC import *
from TPN1.GeneradorPDC import *

from time import time

# ****************************************************************************
# ******************************* Sub - Metodos ******************************
# ****************************************************************************
class ControllerTPN1:

    # ****************************************************************************
    # ***************************** Metodo Principal *****************************
    # ****************************************************************************
    def principalMethod(anchoRollo, ancho, cantidad):
        # Ordenamos los inputs (cantidad:ancho) en base a ancho.
        tupla = list(zip(ancho, cantidad));
        tuplaOrdenada = sorted(tupla, reverse=True);
        ancho, cantidad = zip(*tuplaOrdenada)

        # Generamos y obtenemos los patrones.
        generador = GeneradorPDC(ancho, anchoRollo);
        patrones = generador.generarPatrones();
        # Resolvemos el problema a traves del modelo de programacion lineal.
        ModeloPDC.resolverProblema(patrones, cantidad);


        # ****************************************************************************
        # ****************************************************************************