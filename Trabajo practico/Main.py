from TPN1.ControllerTPN1 import *
from TPN2.ControllerTPN2 import *
from TPN3.ModeloAllInOne import *

import math
import numpy as np
from time import time


# ****************************************************************************
# ***************************** Metodo Principal *****************************
# ****************************************************************************

# Iniciamos el timer
start_time = time()

# Declaracion de inputs.
anchoRollo = 0
cantidad = []
ancho = []

# Lectura de inputs.
f = open('input.txt','r')
message = f.read()                  ## Leemos el archivo
message = message.rsplit('\n')
contador=1
for linea in message:
    if(contador==2):
        anchoRollo = int(linea[0:len(linea)])
    if(contador>=4):
        indice_separator = linea.index(':')
        cant = linea[0:indice_separator]
        anch =  linea[indice_separator+1:len(linea)]
        cantidad.append(int(cant))
        ancho.append(int(anch))
    contador=contador+1
f.close()

# Ordenamos los inputs (cantidad:ancho) en base a ancho.
tupla = list(zip(ancho, cantidad))
tuplaOrdenada = sorted(tupla, reverse=True)
ancho, cantidad = zip(*tuplaOrdenada)

indices = range(len(cantidad))
listIndices = []
for i in indices:
    listIndices.append(i)

indiceCantidad = list(zip(listIndices, cantidad))
indiceAncho = list(zip(listIndices, ancho))

cantidadMaxRollos = 0
cantidadMaxRollos = 0;

for i in indices:
    cantidadMaxRollos = cantidadMaxRollos + math.ceil(cantidad[i]/int(anchoRollo/ancho[i]));

print("* Ancho Rollo: ", anchoRollo)
print("* Ancho: ", ancho)
print("* Cantidad: ", cantidad)
print("* Indice-Cantidad", indiceCantidad)
print("* Indice-Ancho", indiceAncho)
print("* Cantidad-Maxima-De-Rollitos-A-Cortar: ", cantidadMaxRollos)


# ****************************************************************************
# CODE HERE
def executeTPN3():
    ModeloAllInOne.resolver(anchoRollo, indiceAncho, indiceCantidad, cantidadMaxRollos)
    print("Ejecuto TP3");

def InputIsValidForTPN3(anchoRollo, ancho, cantidad):
    valorLimiteDelAnchoRollo = anchoRollo/10
    for a in ancho:
        if(a > valorLimiteDelAnchoRollo):
            return False
    for c in cantidad:
        if(c > valorLimiteDelAnchoRollo):
            return False
    return True

def InputIsValidForTPN1(anchoRollo, ancho, cantidad):
    valorLimiteDelAnchoRollo = anchoRollo/10
    cantidadGrandeConAnchoPequeno = 0
    for i in range(len(ancho)):
        if(ancho[i] < valorLimiteDelAnchoRollo):
            if(cantidad[i] > anchoRollo):
                cantidadGrandeConAnchoPequeno += 1
    if(cantidadGrandeConAnchoPequeno >= 2):
        return False
    return True

if(InputIsValidForTPN3(anchoRollo, ancho, cantidad)):
    executeTPN3()
else:
    if(InputIsValidForTPN1(anchoRollo, ancho, cantidad)):
        print("Ejecuto TP1");
        ControllerTPN1.principalMethod(anchoRollo, ancho, cantidad)
    else:
        ControllerTPN2.principalMethod(anchoRollo, indiceAncho, indiceCantidad, listIndices, cantidad)
        print("Ejecuto TP2");


# ****************************************************************************

execute_time = time() - start_time
print("Tiempo de ejecucion transcurrido : %.10f segundos." % execute_time)

# ****************************************************************************
# ****************************************************************************