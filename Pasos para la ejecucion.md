
# **************************************************************************************************
# EJECUCION DE LOS TPS
# **************************************************************************************************

# **************************************************************************************************
# EJECUCION DEL TP ALGORITMO SELECTOR DE TP MAS EFICIENTE
# **************************************************************************************************

1º Ir a la carpeta Trabajo parctico.
2º Abrir una terminar a esa altura.
3º Ejecutar.
	>_python main.py

NOTA: cambiar en el codigo por el nombre del archivo o viceversa.

# **************************************************************************************************
# EJECUCION INDEPENDIENTE
# **************************************************************************************************

# Ejecucion TPN1

1º Ir a la carpeta Testing -> TPN1.
2º Abrir una terminar a esa altura.
3º Ejecutar.
	>_python main.py

# Ejecucion TPN2

1º Ir a la carpeta Testing -> TPN2.
2º Abrir una terminar a esa altura.
3º Ejecutar.
	>_python main.py

# Ejecucion TPN3

1º Ir a la carpeta Testing -> TPN3.
2º Abrir una terminar a esa altura.
3º Ejecutar.
	>_python main.py

# **************************************************************************************************
